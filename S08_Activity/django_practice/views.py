from django.shortcuts import render, redirect
from django.contrib.auth.models import User
# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict



# def index(request):
#     return HttpResponse("Hello from the views.py file")
from .models import GroceryItem
from django.template import loader

def index(request):
    items = GroceryItem.objects.all()

    context = {
        'items': items,
        'user': request.user,
    }
    return render(request, "django_practice/index.html", context)


def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:    
        user = User()
        user.username = "johndoe"
        user.first_name = "john"
        user.last_name = "doe"
        user.email = "john@gmail.com"
        user.set_password("john1234")
        user.is_staff = False
        user.is_active = True 
        user.save()
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name
        }

    return render(request, "django_practice/register.html", context)

def change_password(request):
    is_user_authenticated = True

    user = authenticate(username="johndoe", password = "john1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "django_practice/change_password.html", context)



def login_view(request):
    username ="johndoe"
    password = "johndoe1"
    user = authenticate(username=username, password=password)
    print(user)

    if user is not None:
        login(request, user)
        return redirect("index")
    else: 
        return render(request, "django_practice/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("index")